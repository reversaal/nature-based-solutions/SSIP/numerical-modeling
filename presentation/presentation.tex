\documentclass[xcolor=table, aspectratio=169]{beamer}
\usetheme{Singapore}
\include{latex_preambule}



\title[Numerical Modeling]{Numerical Modeling of Infiltration Processes}

\begin{document}

% For every picture that defines or uses external nodes, you'll have to
% apply the 'remember picture' style. To avoid some typing, we'll apply
% the style to all pictures.
\tikzstyle{every picture}+=[remember picture]

% By default all math in TikZ nodes are set in inline mode. Change this to
% displaystyle so that we don't get small fractions.
\everymath{\displaystyle}
{
  \usebackgroundtemplate{\includegraphics[width=\paperwidth]{background.png}}
  \begin{frame}[plain]
    \titlepage
    %logo INRAE
    \begin{textblock*}{22mm}[1,1](46mm,92mm)
      \includegraphics[width=22mm]{logo_inrae_etat.png}
    \end{textblock*}
    %logo reseed
    \begin{textblock*}{50mm}[1,1](135mm,90mm)
      \includegraphics[width=15mm]{logo_reseed.png}
    \end{textblock*}
  \end{frame}
}

\usebackgroundtemplate{\includegraphics[width=\paperwidth]{background_2.png}}
% add a title slide at the beginning of each section
\AtBeginSection[]{\frame<beamer>{\frametitle{Contents}\tableofcontents[sectionstyle=show/shaded]}}

% add a slide with the table of contents
\begin{frame}
	\frametitle{Contents} 
	\begin{center}
		\tableofcontents
	\end{center}
\end{frame}

\section[Numerical Methods]{Introduction to numerical methods} % a section will initiate a new column of progress indicator in the header

\begin{frame}
  \frametitle{What is a model?}
   \begin{columns}[t]
 		\begin{column}[c]{6cm}
         \begin{center}
          A model is a symbolic representation of some aspect of a real-world object or phenomenon (Pavé, 2012).
         \end{center}
 		\end{column}
 		\begin{column}[c]{6cm}
         \begin{center}
           \includegraphics[width=0.9\textwidth]{Vaucanson_Duck.jpeg}
           
           \tiny{A. Konby (?), Public domain, via Wikimedia Commons}
         \end{center}
 		\end{column}		
 	\end{columns}
\end{frame}

\begin{frame}
  \frametitle{How to represent a model?}
  A model can be explicited using various tools:
    \begin{itemize}
      \item A formalism (e.g. Forester, 1961)
      \item A mathematical equation (e.g. Partial Differential Equations)
    \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Why using models?}
  \begin{itemize}
    \item understand the behavior of complex systems
    \item optimize sizing and operation
    \item predict performances
  \end{itemize}

  \vfill

  2 Classical pitfalls:
  \begin{itemize}
    \item model and then think
    \item model without expert knowledge
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The different kind of models}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figures/model_classification.png}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Why still using process-based models?}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figures/anderson-end-theory.png}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Not yet obsolete in Environmental Sciences!}
  \begin{tcolorbox}[colback=green!5,colframe=green!40!black,title=Take Home Message]
    \begin{itemize}
      \item Data-driven models are data intensive
      \item Data-driven models are not interpretable
      \item Data-driven models are not transferable (because they are based on correlations and not on causality)
      \item One can use the best of both worlds by combining process-based models with data-driven models
    \end{itemize}
\end{tcolorbox}
\end{frame}

\begin{frame}
  \frametitle{Analytical versus numerical solutions}
  \begin{itemize}
    \item An \textbf{analytical solution} is a closed-form solution to a mathematical problem that provides a continuous function in time and space for a given problem (e.g. Green-Ampt solution for infiltration)
    \item A \textbf{numerical solution} is an approximate solution of a mathematical problem that provides a discrete function in time and space
  \end{itemize}
\end{frame}

\subsection*{} % a subsection will initiate a new line of progress indicator in the header

\begin{frame}
  \frametitle{A short history of numerical methods}
  \begin{center}
    \only<1>{\includegraphics[width=0.7\textwidth]{figures/timeline_1.png}}
    \only<2>{\includegraphics[width=0.7\textwidth]{figures/timeline_2.png}}
    \only<3>{\includegraphics[width=0.7\textwidth]{figures/timeline_3.png}}
    \only<4>{\includegraphics[width=0.7\textwidth]{figures/timeline_4.png}}
    \only<5>{\includegraphics[width=0.7\textwidth]{figures/timeline_5.png}}
    \only<6>{\includegraphics[width=0.7\textwidth]{figures/timeline_6.png}}
    \only<7>{\includegraphics[width=0.7\textwidth]{figures/timeline_7.png}}
    \only<8>{\includegraphics[width=0.7\textwidth]{figures/timeline_8.png}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{A short history of numerical methods}
  \begin{itemize}
    \item 80s: finite volume methods
    \item nowadays: discontinuous Galerkin methods, Lattice Boltzmann methods and quantic computers
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The different numerical methods}
  \begin{center}
    \includegraphics[width=0.9\textwidth]{figures/comparison_methods.png}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Boundary and initial conditions}
  \begin{itemize}
    \item PDEs (and ODEs) do not have unique solutions
    \item Boundary and initial conditions are required to solve the equations
    \item There are different types of boundary conditions, the two main ones are:
      \begin{itemize}
        \item Dirichlet boundary conditions
        \item Neumann boundary conditions
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Boundary and initial conditions}

  \begin{columns}[t]
    \begin{column}[c]{6cm}
      A simple example:
      \begin{equation}
        \frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2}
      \end{equation}
      with:
      \begin{itemize}
        \item $u(x,0) = f(x)$
        \item $u(a,t) = 0$
        \item $u(b,t) = 0$
      \end{itemize}
    \end{column}
    \begin{column}[c]{6cm}
        \begin{center}
          \includegraphics[width=0.9\textwidth]{boundary_conditions.png}
        \end{center}
    \end{column}		
  \end{columns}
  
  \begin{center}
    \small{Source: \href{https://services.math.duke.edu/~jtwong/math356-2019/lectures/PDE3_eigenfunctions.pdf}{J. Wong, Duke University}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Popular tools for solving the Richard's equation}
  \begin{itemize}
    \item Hydrus (\href{https://www.pc-progress.com/en/Default.aspx}{https://www.pc-progress.com/en/Default.aspx}):
      \begin{itemize}
        \item focused on soil science (appropriate boundary conditions)
        \item finite elements
        \item active community
      \end{itemize}
    \item RichardsFOAM (\href{https://develop.openfoam.com/Community/hydrology/}{https://develop.openfoam.com/Community/hydrology/}):
      \begin{itemize}
        \item based on OpenFOAM
        \item finite volumes
        \item few applications
      \end{itemize}
    \item COMSOL (\href{https://www.comsol.fr/}{https://www.comsol.fr/})
      \begin{itemize}
        \item finite elements
        \item general purpose (multiphysics)
      \end{itemize}
  \end{itemize}
\end{frame}

%\subsection*{}
\section[Hands-on 1]{Infiltration under a constant flux}

\begin{frame}
  \frametitle{Hands-on 1: Infiltration under a constant flux}
  \framesubtitle{Description of the problem}
  \begin{center}
    \includegraphics[width=0.4\textwidth]{hands-on-1.png}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Create Project}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive1.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive2.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive3.PNG}}
    \only<4>{\includegraphics[width=0.85\textwidth]{Diapositive4.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Parameter table}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive5.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive5_bis.png}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Build geometry}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive6.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive7.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Add materials}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive8.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive9.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Parameter physics}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive10.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive11.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive12.PNG}}
    \only<4>{\includegraphics[width=0.85\textwidth]{Diapositive13.PNG}}
    \only<5>{\includegraphics[width=0.85\textwidth]{Diapositive_13bis.png}}
    \only<6>{\includegraphics[width=0.85\textwidth]{Diapositive14.PNG}}
    \only<7>{\includegraphics[width=0.85\textwidth]{Diapositive15.PNG}}
    \only<8>{\includegraphics[width=0.85\textwidth]{Diapositive16.PNG}}
    \only<9>{\includegraphics[width=0.85\textwidth]{Diapositive17.PNG}}
    \only<10>{\includegraphics[width=0.85\textwidth]{Diapositive17_bis.png}}
    \only<11>{\includegraphics[width=0.85\textwidth]{Diapositive18.PNG}}
    \only<12>{\includegraphics[width=0.85\textwidth]{Diapositive19.PNG}}
    \only<13>{\includegraphics[width=0.85\textwidth]{Diapositive20.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Mesh}
  \begin{center}
    \includegraphics[width=0.85\textwidth]{Diapositive21.PNG}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Study}
  \begin{center}
    \includegraphics[width=0.85\textwidth]{Diapositive22.PNG}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{itemize}
    \item Surface plot
  \end{itemize}
  \begin{center}
    \only<1>{\includegraphics[width=0.80\textwidth]{Diapositive23.PNG}}
    \only<2>{\includegraphics[width=0.80\textwidth]{Diapositive24.PNG}}
    \only<3>{\includegraphics[width=0.80\textwidth]{Diapositive25.PNG}}
    \only<4>{\includegraphics[width=0.80\textwidth]{Diapositive26.PNG}}
    \only<5>{\includegraphics[width=0.80\textwidth]{Diapositive27.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{itemize}
    \item Line plot
  \end{itemize}
  \begin{center}
    \only<1>{\includegraphics[width=0.80\textwidth]{Diapositive28.PNG}}
    \only<2>{\includegraphics[width=0.80\textwidth]{Diapositive29.PNG}}
    \only<3>{\includegraphics[width=0.80\textwidth]{Diapositive30.PNG}}
    \only<4>{\includegraphics[width=0.80\textwidth]{Diapositive31.PNG}}
    \only<5>{\includegraphics[width=0.80\textwidth]{Diapositive32.PNG}}
    \only<6>{\includegraphics[width=0.80\textwidth]{Diapositive33.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Results}
  \begin{itemize}
    \item Mass balance
  \end{itemize}
  \begin{center}
    \only<1>{\includegraphics[width=0.8\textwidth]{Diapositive34.PNG}}
    \only<2>{\includegraphics[width=0.8\textwidth]{Diapositive35.PNG}}
    \only<3>{\includegraphics[width=0.8\textwidth]{Diapositive36.PNG}}
    \only<4>{\includegraphics[width=0.8\textwidth]{Diapositive37.PNG}}
    \only<5>{\includegraphics[width=0.8\textwidth]{Diapositive38.PNG}}
    \only<6>{\includegraphics[width=0.8\textwidth]{Diapositive39.PNG}}
    \only<7>{\includegraphics[width=0.8\textwidth]{Diapositive40.PNG}}
    \only<8>{\includegraphics[width=0.8\textwidth]{Diapositive41.PNG}}
    \only<9>{\includegraphics[width=0.8\textwidth]{Diapositive42.PNG}}
    \only<10>{\includegraphics[width=0.8\textwidth]{Diapositive43.PNG}}
    \only<11>{\includegraphics[width=0.8\textwidth]{Diapositive44.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Finite elements and oscillations}
  \begin{itemize}
    \item Finite elements enforce the continuity of the variable at the nodes
    \item Oscillations can appear in case of steep gradients
  \end{itemize}

  \begin{columns}[t]
    \begin{column}[c]{6cm}
      \begin{center}
        $$ Pe = \frac{\text{advective transport rate}}{\text{diffusive transport rate}} $$
      \end{center}
    \end{column}
    \begin{column}[c]{6cm}
      \begin{center}
        $$ Pe(h) = \frac{1}{K}\frac{d K}{d h} \Delta z $$ (El-Kadi and Ling, 1993)
      \end{center}        
    \end{column}		
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Finite elements and oscillations}
  There are three stragegies to reduce oscillations:
  \begin{itemize}
    \item refine the mesh
    \item use stabilization techniques
    \item use higher order elements or alternative discretization schemes
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Mesh refinement 1/3}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive45.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive46.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Mesh refinement 2/3}
  \begin{center}
    \includegraphics[width=0.85\textwidth]{sketch_hands-on_2.png}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Mesh refinement 3/3}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive47.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive48.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive49.PNG}}
    \only<4>{\includegraphics[width=0.85\textwidth]{Diapositive50.PNG}}
    \only<5>{\includegraphics[width=0.85\textwidth]{Diapositive51.PNG}}
  \end{center}
\end{frame}

\section[Hands-on 2]{The infiltration experiment}

\begin{frame}
  \frametitle{Hands-on 2: Infiltration experiment}
\end{frame}

\begin{frame}
  \frametitle{Create Project}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive_2_1.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive_2_2.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive_2_3.PNG}}
    \only<4>{\includegraphics[width=0.85\textwidth]{Diapositive_2_4.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Parameter table}
  \begin{center}
    \includegraphics[width=0.85\textwidth]{Diapositive_2_5.PNG}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Build geometry}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive_2_6.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive_2_7.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive_2_8.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Add materials}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive_2_9.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive_2_10.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Parameter physics}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive_2_11.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive_2_12.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive_2_13.PNG}}
    \only<4>{\includegraphics[width=0.85\textwidth]{Diapositive_2_14.PNG}}
    \only<5>{\includegraphics[width=0.85\textwidth]{Diapositive_2_15.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Mesh}
  \begin{itemize}
    \item Generate a \textbf{finer} mesh
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Study}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive_2_16.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive_2_17.PNG}}
    \only<3>{\includegraphics[width=0.85\textwidth]{Diapositive_2_18.PNG}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Infiltration rate}
  \begin{center}
    \only<1>{\includegraphics[width=0.85\textwidth]{Diapositive_2_23.PNG}}
    \only<2>{\includegraphics[width=0.85\textwidth]{Diapositive_2_24.PNG}}
  \end{center}
\end{frame}

\subsection*{}

\begin{frame}
  \frametitle{Sweeping the parameter space}
  \begin{center}
    \includegraphics[width=0.85\textwidth]{Diapositive_2_19.PNG}
  \end{center}
\end{frame}

\end{document}