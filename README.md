# Numerical Modeling of Infiltration Processes

![License: CC BY 4.0](https://img.shields.io/badge/Licenses-CC_BY_4.0-green)

This repository contains materials for the course "Numerical Modeling of Infiltration Processes" part of the [Eco-Campus Summer School on Infiltration processes](https://forgemia.inra.fr/reversaal/nature-based-solutions/SSIP) co-organized by [INRAE](www.inrae.fr) and [INSA Lyon](www.insa-lyon.fr).

## Usage

The folder 'presentation' contains the slides of the course and the folder 'models' contains the COMSOL mph files used in the course.

## Citation


FORQUET, NICOLAS; CLEMENT, REMI; LASSABATERE, LAURENT; LOUIS, PAULINE, 2024, "Eco-campus summer school on infiltration processes", https://doi.org/10.57745/3QYCIY, Recherche Data Gouv, V1 

## Acknowledgment

This work utilized COMSOL Multiphysics® 6.2, a commercial software for engineering simulation. The simulations presented in this repository were performed using the Subsurface flow module of COMSOL Multiphysics®. The authors acknowledge COMSOL AB for providing the necessary software support to accomplish this work.

The authors would also like to thank Zoé Legeai for her help in preparing the course materials.
